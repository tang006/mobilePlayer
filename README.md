## 欢迎使用MobilePlayer

*最近更新时间：2017-12-14*

**MobilePlayer**是一款专为微信浏览器打造的依赖于jQuery的移动端视频播放器。

本文档是介绍**MobilePlayer**的使用说明，它可帮助用户直接使用经过验证的视频播放能力，通过灵活的终端兼容，快速融入自有项目，以实现在微信浏览器中的播放功能。

该文档面向考虑使用**MobilePlayer**播放器进行开发并具备JavaScript语言基础的开发人员。 

-------------------

[TOC]

### 能力预览
#### 格式支持

|   播放格式   | 支持情况 |
| :---------: | :-----: |
|   MP4       |   `✔`  |
|   HLS(m3u8) |   `✔`  |
|   RTMP      |   `✘`  |

#### 终端兼容

- 鉴于微信浏览器X5内核下的同层播放器专为Android系统开发，故本播放器对Android系统更为亲和，支持**全屏模式下横竖屏的自由切换**以及**视频头部标题栏的自定义**。
- 对于IOS系统而言，我们使用IOS默认的全屏效果，但这并不妨碍IOS系统自动切换横竖屏，后续版本中，我们也会逐步支持IOS的横屏效果。另外由于权限问题，IOS系统下无静音设置按钮。
- 播放器内部会自动区分终端使用最优的播放方案。

### 准备工作
#### 创建步骤

- 添加播放器容器

```
<section id="player-wrapper"></section>
```

- 创建Player对象

```
var player = new LePlayer({
	wrapperId: 'player-wrapper',
	title: 'LePlayer',
	urls: [
		{title: '地址一',url: 'http://m3u8地址1.m3u8'},
        {title: '地址二',url: 'http://m3u8地址2.m3u8'},
        {title: '地址三',url: 'http://m3u8地址3.m3u8'}
	],
	isLive: true,
	isLoop: true,
	poster: './images/poster.jpg'
});
```

#### 完整实例代码

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="./css/player.css">
    <script src="./js/jquery.min.js"></script>
    <script src="./js/player.js"></script>
</head>
<body>
    <section id="player-wrapper"></section>
    <section>
        页面其他内容
    </section>
    <script>
        var params = {
            wrapperId: 'player-wrapper',
            title: 'LePlayer',
            urls: [
                // m3u8地址
                {title: '地址一',url: 'http://m3u8地址1.m3u8'},
                {title: '地址二',url: 'http://m3u8地址2.m3u8'},
                {title: '地址三',url: 'http://m3u8地址3.m3u8'},
                
                // MP4地址
                // {url:'http://www.w3school.com.cn/example/html5/mov_bbb.mp4'}
            ],
            isLive: true,
            isLoop: false,
            poster: './images/poster.jpg'
        };
        var player = new LePlayer(params);
    </script>
</body>
</html>
```

### API一览
#### 构造函数

```
var player = new LePlayer(params);
```

#### params: Object 参数设置
 
| 参数        | 类型    | 默认值    |  描述 |
| :--------- | :------ | :------- | :--- |
| wrapperId  | String  | 无       | 必选。设置播放器容器的ID。|
| title      | String  | Document | 可选。设置播放器默认头部标题文字。*该参数仅在Android系统下有效。* |
| urls       | Array   | 无       | 必选。设置一个或多个播放源，用于在播放不稳定时切换播放源。每个播放源通过options配置，具体见下表。 |
| isLive     | Boolean | false    | 可选。设置播放器状态为直播或者点播。 |
| isLoop     | Boolean | false    | 可选。设置当前视频是否循环播放。*该参数仅在播放器状态为点播时有效。* |
| poster     | String  | defaultImg | 可选。设置当前视频的预览图。 |

#### options: Object 参数设置

| 参数       | 类型    | 默认值    |  描述 |
| :-------- | :------ | :------- | :--- |
| title     | String  | 无       | 必选。设置播放源标题，用于换源提示文字。|
| url       | String  | 无       | 必选。设置播放源地址。|

> 点播与直播状态下界面的区别在于有无快进快退按钮，是否显示当前时间与总时长，有无播放进度条以及是否显示换源提示（设置一个以上的播放地址，在不可播放状态持续5s显示）。

#### 方法

**player.init()方法**

> - 语法示例
> player.init(function(data){console.log(data)});

| 参数      | 描述 |
| :------- | :--- |
| function | 必选。规定当媒体数据加载完成时执行的函数，参数data（object）为返回的数据元信息。*参数在初次点击播放后返回。* |

**player.printScreen()方法**

> - 语法示例
> player.printScreen();

| 返回值 | 描述 |
| :---- | :--- |
| data  | 返回截图的Base64编码。 |

**player.listenStatus()方法**

> - 语法示例
> player.listenStatus(event,function);

| 参数      | 描述 |
| :------- | :--- |
| event    | 必选。所监听的事件类型，目前支持'play'、'pause'以及'error'。|
| function | 必选。规定该事件被触发时执行的函数。|

**player.setPlayerTitle()方法** *该方法仅在Android系统下有效。*

> - 语法示例
> player.setPlayerTitle(title);

| 参数  | 描述 |
| :---- | :--- |
| title | 必选。设置播放器当前头部标题文字。 |

**player.currentTime()方法**

> - 语法示例
> player.currentTime(time);

| 参数  | 描述 |
| :---- | :--- |
| time  | 可选。无参数时可返回当前的播放位置（单位s），有参数（正实数）时设置播放位置并播放。 |

**感谢您阅读本文档。**
